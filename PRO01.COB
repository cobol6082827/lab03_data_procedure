       IDENTIFICATION  DIVISION.
       PROGRAM-ID. PRO01.
       AUTHOR. Waritphat.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  PROBLEM-STR PIC   X(50).
       01  NUM1       PIC   99.
       01  NUM2       PIC   99.
       01  NUM3       PIC   99.
       01  NUM4       PIC   99.
       
       PROCEDURE DIVISION.
           PERFORM PROBLEM-01
           PERFORM PROBLEM-02
           PERFORM PROBLEM-03
           PERFORM PROBLEM-04
           PERFORM PROBLEM-05
           PERFORM PROBLEM-06
           PERFORM PROBLEM-07 
           PERFORM PROBLEM-08
           PERFORM PROBLEM-09
           PERFORM PROBLEM-10
           PERFORM PROBLEM-11
           PERFORM PROBLEM-12
           GOBACK.
       
       PROBLEM-01.
           MOVE "PROBLEM-01: ADD NUM1 TO NUM2" TO PROBLEM-STR.
           MOVE  25    TO    NUM1.
           MOVE  30    TO    NUM2.
           MOVE  00    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           ADD NUM1 TO NUM2.
           PERFORM DISPLAY-AFTER.
           EXIT
           .
       PROBLEM-02.
           MOVE "PROBLEM-02: ADD NUM1, NUM2 TO NUM3, NUM4" 
           TO PROBLEM-STR.
           MOVE  13    TO    NUM1.
           MOVE  04    TO    NUM2.
           MOVE  05    TO    NUM3.
           MOVE  12    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           ADD NUM1, NUM2 TO NUM3, NUM4.
           PERFORM DISPLAY-AFTER.
           EXIT
           .
       PROBLEM-03.
           MOVE "PROBLEM-03: ADD NUM1, NUM2 ,NUM3 GIVING NUM4" 
           TO PROBLEM-STR.
           MOVE  04    TO    NUM1.
           MOVE  03    TO    NUM2.
           MOVE  02    TO    NUM3.
           MOVE  01    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           ADD NUM1, NUM2 ,NUM3 GIVING NUM4.
           PERFORM DISPLAY-AFTER.
           EXIT
           .       
       PROBLEM-04.
           MOVE "PROBLEM-04: SUBTRACT NUM1 FROM NUM2 GIVING NUM3" 
           TO PROBLEM-STR.
           MOVE  04    TO    NUM1.
           MOVE  10    TO    NUM2.
           MOVE  55    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1 FROM NUM2 GIVING NUM3.
           PERFORM DISPLAY-AFTER.
           EXIT
           . 
       PROBLEM-05.
           MOVE "PROBLEM-05: SUBTRACT NUM1, NUM2 FROM NUM3" 
           TO PROBLEM-STR.
           MOVE  05    TO    NUM1.
           MOVE  10    TO    NUM2.
           MOVE  55    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1, NUM2 FROM NUM3.
           PERFORM DISPLAY-AFTER.
           EXIT
           . 
       PROBLEM-06.
           MOVE "PROBLEM-06: SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4" 
           TO PROBLEM-STR.
           MOVE  05    TO    NUM1.
           MOVE  10    TO    NUM2.
           MOVE  55    TO    NUM3.
           MOVE  20    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4.
           PERFORM DISPLAY-AFTER.
           EXIT
           . 
       PROBLEM-07.
           MOVE "PROBLEM-07: MULTIPLY NUM1 BY NUM2" 
           TO PROBLEM-STR.
           MOVE  10    TO    NUM1.
           MOVE  05    TO    NUM2.
           MOVE  00    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           MULTIPLY NUM1 BY NUM2.
           PERFORM DISPLAY-AFTER.
           EXIT
           .
       PROBLEM-08.
           MOVE "PROBLEM-08: MULTIPLY NUM1 BY NUM2 GIVING NUM3" 
           TO PROBLEM-STR.
           MOVE  10    TO    NUM1.
           MOVE  05    TO    NUM2.
           MOVE  33    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           MULTIPLY NUM1 BY NUM2 GIVING NUM3.
           PERFORM DISPLAY-AFTER.
           EXIT
           .
       PROBLEM-09.
           MOVE "PROBLEM-09: DIVIDE NUM1 INTO NUM2" 
           TO PROBLEM-STR.
           MOVE  05    TO    NUM1.
           MOVE  64    TO    NUM2.
           MOVE  00    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           DIVIDE NUM1 INTO NUM2.
           PERFORM DISPLAY-AFTER.
           EXIT
           .     
       PROBLEM-10.
           MOVE "PROBLEM-10: DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER 
      -    "NUM4" TO PROBLEM-STR.
           MOVE  05    TO    NUM1.
           MOVE  64    TO    NUM2.
           MOVE  24    TO    NUM3.
           MOVE  88    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4.
           PERFORM DISPLAY-AFTER.
           EXIT
           .
       PROBLEM-11.
           MOVE "PROBLEM-11: COMPUTE NUM1 = 5 + 10 * 30 / 2"
           TO PROBLEM-STR.
           MOVE  25    TO    NUM1.
           MOVE  00    TO    NUM2.
           MOVE  00    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           COMPUTE NUM1 = 5 + (10 * 30 / 2).
           PERFORM DISPLAY-AFTER.
           EXIT
           .   
       PROBLEM-12.
           MOVE "PROBLEM-12: COMPUTE NUM1 = 99+1"
           TO PROBLEM-STR.
           MOVE  25    TO    NUM1.
           MOVE  00    TO    NUM2.
           MOVE  00    TO    NUM3.
           MOVE  00    TO    NUM4.
           PERFORM HEADER.
           PERFORM DISPLAY-BEFORE.
           COMPUTE NUM1 = 99 + 1 ON SIZE ERROR 
           DISPLAY "SIZE ERROR."
           END-COMPUTE
           PERFORM DISPLAY-AFTER.
           EXIT
           .        
       HEADER.
           DISPLAY "==================================================".
           DISPLAY PROBLEM-STR.
           DISPLAY "          NUM1     NUM2     NUM3     NUM4"
           .
       DISPLAY-BEFORE.
           DISPLAY "Before |    "NUM1"       "NUM2"       "NUM3
           "       "NUM4 
           .
       DISPLAY-AFTER.
           DISPLAY "After  |    "NUM1"       "NUM2"       "NUM3
           "       "NUM4  
           .


